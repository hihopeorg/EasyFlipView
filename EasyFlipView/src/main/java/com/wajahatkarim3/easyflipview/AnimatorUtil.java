/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wajahatkarim3.easyflipview;

import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

import java.util.ArrayList;

public class AnimatorUtil {
    private EasyFlipView easyFlipView;
    private ArrayList<AnimatorProperty> animatorGroupArrayList;
    public static final int NUM = 3;

    public AnimatorUtil(EasyFlipView easyFlip) {
        this.easyFlipView = easyFlip;
    }

    public AnimatorGroup animFlipHorizontalOut(Component component,int duration) {
        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroupArrayList = initAnimFlip(NUM, component);
        animatorGroupArrayList.get(0).rotate(-360).setDuration(0 == duration ? duration : 500);
        animatorGroupArrayList.get(1).alphaFrom(1).alpha(0).setDelay(0 == duration ? duration/2 : 250).setDuration(0);
        animatorGroup.runSerially(animatorGroupArrayList.get(0), animatorGroupArrayList.get(1), animatorGroupArrayList.get(2));
        return animatorGroup;
    }

    public AnimatorGroup animFlipHorizontalIn(Component component,int duration) {
        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroupArrayList = initAnimFlip(NUM, component);
        animatorGroupArrayList.get(0).alphaFrom(1).alpha(0).setDuration(0);
        animatorGroupArrayList.get(1).rotate(360).setDuration(0 == duration ? duration : 500);
        animatorGroupArrayList.get(2).alphaFrom(0).alpha(1).setDelay(0 == duration ? duration/2 : 250).setDuration(0);
        animatorGroup.runSerially(animatorGroupArrayList.get(0), animatorGroupArrayList.get(1), animatorGroupArrayList.get(2));
        return animatorGroup;
    }

    public AnimatorGroup animFlipHorizontalRightOut(Component component,int duration) {
        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroupArrayList = initAnimFlip(NUM, component);
        animatorGroupArrayList.get(0).rotate(-360).setDuration(0 == duration ? duration : 3000);
        animatorGroupArrayList.get(1).alphaFrom(1).alpha(0).setDelay(0 == duration ? duration/2 : 1500).setDuration(0);
        animatorGroup.runSerially(animatorGroupArrayList.get(0), animatorGroupArrayList.get(1), animatorGroupArrayList.get(2));
        return animatorGroup;
    }

    public AnimatorGroup animFlipHorizontalRightIn(Component component,int duration) {
        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroupArrayList = initAnimFlip(NUM, component);
        animatorGroupArrayList.get(0).alphaFrom(1).alpha(0).setDuration(0);
        animatorGroupArrayList.get(1).rotate(360).setDuration(0 == duration ? duration : 3000);
        animatorGroupArrayList.get(2).alphaFrom(0).alpha(1).setDelay(0 == duration ? duration/2 : 1500).setDuration(1);
        animatorGroup.runSerially(animatorGroupArrayList.get(0), animatorGroupArrayList.get(1), animatorGroupArrayList.get(2));
        return animatorGroup;
    }


    public AnimatorGroup animFlipVerticalFrontOut(Component component,int duration) {
        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroupArrayList = initAnimFlip(NUM, component);
        animatorGroupArrayList.get(0).rotate(-360).setDuration(0 == duration ? duration : 500);
        animatorGroupArrayList.get(1).alphaFrom(1).alpha(0).setDelay(0 == duration ? duration/2 : 250).setDuration(0);
        animatorGroup.runSerially(animatorGroupArrayList.get(0), animatorGroupArrayList.get(1), animatorGroupArrayList.get(2));
        return animatorGroup;
    }

    public AnimatorGroup animFlipVerticalFrontIn(Component component,int duration) {
        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroupArrayList = initAnimFlip(NUM, component);
        animatorGroupArrayList.get(0).alphaFrom(1).alpha(0).setDuration(0);
        animatorGroupArrayList.get(1).rotate(360).setDuration(0 == duration ? duration : 500);
        animatorGroupArrayList.get(2).alphaFrom(0).alpha(1).setDelay(0 == duration ? duration/2 : 250).setDuration(0);
        animatorGroup.runSerially(animatorGroupArrayList.get(0), animatorGroupArrayList.get(1), animatorGroupArrayList.get(2));
        return animatorGroup;
    }

    public AnimatorGroup animFlipVerticalOut(Component component,int duration) {
        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroupArrayList = initAnimFlip(NUM, component);
        animatorGroupArrayList.get(0).rotate(-360).setDuration(0 == duration ? duration : 500);
        animatorGroupArrayList.get(1).alphaFrom(1).alpha(0).setDelay(0 == duration ? duration/2 : 250).setDuration(0);
        animatorGroup.runSerially(animatorGroupArrayList.get(0), animatorGroupArrayList.get(1), animatorGroupArrayList.get(2));
        return animatorGroup;
    }

    public AnimatorGroup animFlipVerticalIn(Component component,int duration) {
        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroupArrayList = initAnimFlip(NUM, component);
        animatorGroupArrayList.get(0).alphaFrom(1).alpha(0).setDuration(0);
        animatorGroupArrayList.get(1).rotate(360).setDuration(0 == duration ? duration : 500);
        animatorGroupArrayList.get(2).alphaFrom(0).alpha(1).setDelay(0 == duration ? duration/2 : 250).setDuration(0);
        animatorGroup.runSerially(animatorGroupArrayList.get(0), animatorGroupArrayList.get(1), animatorGroupArrayList.get(2));
        return animatorGroup;
    }

    public ArrayList<AnimatorProperty> initAnimFlip(int num, Component component) {
        ArrayList<AnimatorProperty> animatorGroupArrayList = new ArrayList<AnimatorProperty>();
        AnimatorProperty animator;
        for (int i = 0; i < num; i++) {
            animator = easyFlipView.createAnimatorProperty();
            animator.setTarget(component);
            animatorGroupArrayList.add(animator);
        }
        return animatorGroupArrayList;
    }
}
