/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wajahatkarim3.easyflipview;

import ohos.agp.components.AttrSet;

public class AttrUtil {
    private AttrSet attrSet;

    public static final String autoFlipBackKey = "autoFlipBack";
    public static final String autoFlipBackTimeKey = "autoFlipBackTime";
    public static final String flipDurationKey = "flipDuration";
    public static final String flipOnceEnabledKey = "flipOnceEnabled";
    public static final String flipEnabledKey = "flipEnabled";
    public static final String flipFromKey = "flipFrom";
    public static final String flipOnTouchKey = "flipOnTouch";
    public static final String flipTypeKey = "flipType";

    public AttrUtil(AttrSet attrSet) {
        this.attrSet = attrSet;
    }

    public String getStringValue(String key, String defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getStringValue();
        } else {
            return defValue;
        }
    }

    public int getInt(String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    public boolean getBoolean(String key, boolean defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getBoolValue();
        } else {
            return defValue;
        }
    }

    public static boolean isEmpty(CharSequence str) {
        if (str == null || str.length() == 0)
            return true;
        else
            return false;
    }
}
