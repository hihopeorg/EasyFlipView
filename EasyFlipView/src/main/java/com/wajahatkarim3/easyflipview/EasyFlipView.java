package com.wajahatkarim3.easyflipview;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;


/**
 * A quick and easy flip view through which you can create views with two sides like credit cards,
 * poker cards, flash cards etc.
 * <p>
 * Add com.wajahatkarim3.easyflipview.EasyFlipView into your XML layouts with two direct children
 * views and you are done!
 * For more information, check http://github.com/wajahatkarim3/EasyFlipView
 *
 * @author Wajahat Karim (http://wajahatkarim.com)
 * @version 3.0.0 28/03/2019
 */
public class EasyFlipView extends StackLayout implements Component.EstimateSizeListener, ComponentContainer.ArrangeListener, Component.TouchEventListener {

    public static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "EasyFlipView");

    public static final int DEFAULT_FLIP_DURATION = 400;
    public static final int DEFAULT_AUTO_FLIP_BACK_TIME = 1000;
    public boolean isFirst = true;

    public enum FlipState {
        FRONT_SIDE, BACK_SIDE
    }

    private AnimatorGroup mSetRightOut;
    private AnimatorGroup mSetLeftIn;
    private AnimatorGroup mSetTopOut;
    private AnimatorGroup mSetBottomIn;
    private boolean mIsBackVisible = false;
    private Component mCardFrontLayout;
    private Component mCardBackLayout;
    private String flipType = "vertical";
    private String flipTypeFrom = "right";

    private boolean flipOnTouch;
    private int flipDuration = 0;
    private boolean flipEnabled;
    private boolean flipOnceEnabled;
    private boolean autoFlipBack;
    private int autoFlipBackTime;

    private Context context;
    private float x1;
    private float y1;

    private FlipState mFlipState = FlipState.FRONT_SIDE;

    private OnFlipAnimationListener onFlipListener = null;

    private AttrUtil attrUtil;
    private AnimatorUtil animatorUtil;
    private EventHandler eventHandler;

    public EasyFlipView(Context context) {
        this(context, null);
    }


    public EasyFlipView(Context context, AttrSet attrs) {
        super(context, attrs);
        setEstimateSizeListener(this);
        setArrangeListener(this);

        new AnimatorUtil(this);
        this.context = context;
        attrUtil = new AttrUtil(attrs);
        animatorUtil = new AnimatorUtil(this);
        setTouchEventListener(this::onTouchEvent);
        initAttr(context, attrs);
    }

    public void initAttr(Context context, AttrSet attrs) {
        // Check for the attributes
        if (attrs != null) {
            flipOnTouch = attrUtil.getBoolean(AttrUtil.flipOnTouchKey, true);
            flipDuration = attrUtil.getInt(AttrUtil.flipDurationKey, DEFAULT_FLIP_DURATION);
            flipEnabled = attrUtil.getBoolean(AttrUtil.flipEnabledKey, true);
            flipOnceEnabled = attrUtil.getBoolean(AttrUtil.flipOnceEnabledKey, false);
            autoFlipBack = attrUtil.getBoolean(AttrUtil.autoFlipBackKey, false);
            autoFlipBackTime = attrUtil.getInt(AttrUtil.autoFlipBackTimeKey, DEFAULT_AUTO_FLIP_BACK_TIME);
            flipType = attrUtil.getStringValue(AttrUtil.flipTypeKey, "vertical");
            flipTypeFrom = attrUtil.getStringValue(AttrUtil.flipFromKey, "right");
        }
        HiLog.warn(label, "initAttr toEasyFlipViewString " + toEasyFlipViewString());
    }

    private void initView() {
        if (isFirst == true) {
            mCardFrontLayout.setVisibility(VISIBLE);
            if (mCardBackLayout != null) {
                mCardBackLayout.setVisibility(INVISIBLE);
            }
            isFirst = false;
        }
    }

    @Override
    public boolean onArrange(int left, int top, int right, int bottom) {
        HiLog.warn(label, "onArrange" + left + " " + top + " " + right + " " + bottom);
        return false;
    }

    @Override
    public boolean onEstimateSize(int width, int height) {
        onFinishInflate();
        return false;
    }


    protected void onFinishInflate() {
        if (getChildCount() > 2) {
            throw new IllegalStateException("EasyFlipView can host only two direct children!");
        }

        findViews();
        loadAnimations();
        changeCameraDistance();
    }

    @Override
    public void addComponent(Component childComponent, int index, ComponentContainer.LayoutConfig layoutConfig) {
        super.addComponent(childComponent, index, layoutConfig);
        if (getChildCount() == 2) {
            throw new IllegalStateException("EasyFlipView can host only two direct children!");
        }
        findViews();
        changeCameraDistance();
    }

    @Override
    public void removeComponent(Component component) {
        super.removeComponent(component);
        findViews();
    }

    @Override
    public void removeAllComponents() {
        super.removeAllComponents();
        // Reset the state
        mFlipState = FlipState.FRONT_SIDE;
        findViews();
    }

    private void findViews() {
        // Invalidation since we use this also on removeView
        mCardBackLayout = null;
        mCardFrontLayout = null;
        int childs = getChildCount();
        if (childs < 1) {
            return;
        }

        if (childs < 2) {
            // Only invalidate flip state if we have a single child
            mFlipState = FlipState.FRONT_SIDE;
            mCardFrontLayout = getComponentAt(0);
        } else if (childs == 2) {
            mCardFrontLayout = getComponentAt(1);
            mCardBackLayout = getComponentAt(0);
        }
        if (isFirst == true) {
            initView();
            isFirst = false;
        }
    }

    private void setupInitializations() {
        mCardBackLayout.setVisibility(INVISIBLE);
    }

    private void loadAnimations() {
        if (flipType.equalsIgnoreCase("horizontal")) {
            if (flipTypeFrom.equalsIgnoreCase("left")) {

                mSetRightOut = animatorUtil.animFlipHorizontalOut(mCardFrontLayout, flipDuration);
                mSetLeftIn = animatorUtil.animFlipHorizontalIn(mCardBackLayout, flipDuration);
            } else {
                mSetRightOut = animatorUtil.animFlipHorizontalRightOut(mCardFrontLayout, flipDuration);
                mSetLeftIn = animatorUtil.animFlipHorizontalRightIn(mCardBackLayout, flipDuration);
            }

            if (mSetRightOut == null || mSetLeftIn == null) {
                throw new RuntimeException(
                        "No Animations Found! Please set Flip in and Flip out animation Ids.");
            }

            mSetRightOut.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    if (mFlipState == FlipState.FRONT_SIDE) {
                        mCardBackLayout.setVisibility(INVISIBLE);
                        mCardFrontLayout.setVisibility(VISIBLE);
                        if (onFlipListener != null)
                            onFlipListener.onViewFlipCompleted(EasyFlipView.this, FlipState.FRONT_SIDE);
                    } else {
                        mCardBackLayout.setVisibility(VISIBLE);
                        mCardFrontLayout.setVisibility(INVISIBLE);
                        if (onFlipListener != null)
                            onFlipListener.onViewFlipCompleted(EasyFlipView.this, FlipState.BACK_SIDE);

                        // Auto Flip Back
                        if (autoFlipBack == true) {
                            eventHandler = new EventHandler(EventRunner.getMainEventRunner());
                            eventHandler.postTimingTask(new Runnable() {
                                @Override
                                public void run() {
                                    flipTheView();
                                }
                            }, autoFlipBackTime);
                        }
                    }
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            setFlipDuration(flipDuration);
        } else {
            if (!AttrUtil.isEmpty(flipTypeFrom) && flipTypeFrom.equalsIgnoreCase("front")) {
                mSetTopOut = animatorUtil.animFlipVerticalFrontOut(mCardFrontLayout, flipDuration);
                mSetBottomIn = animatorUtil.animFlipVerticalFrontIn(mCardBackLayout, flipDuration);
            } else {
                mSetTopOut = animatorUtil.animFlipVerticalOut(mCardFrontLayout, flipDuration);
                mSetBottomIn = animatorUtil.animFlipVerticalIn(mCardBackLayout, flipDuration);
            }

            if (mSetTopOut == null || mSetBottomIn == null) {
                throw new RuntimeException(
                        "No Animations Found! Please set Flip in and Flip out animation Ids.");
            }

            mSetTopOut.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                }

                @Override
                public void onStop(Animator animator) {
                }

                @Override
                public void onCancel(Animator animator) {
                }

                @Override
                public void onEnd(Animator animator) {
                    if (mFlipState == FlipState.FRONT_SIDE) {
                        mCardBackLayout.setVisibility(INVISIBLE);
                        mCardFrontLayout.setVisibility(VISIBLE);

                        if (onFlipListener != null)
                            onFlipListener.onViewFlipCompleted(EasyFlipView.this, FlipState.FRONT_SIDE);
                    } else {
                        mCardBackLayout.setVisibility(VISIBLE);
                        mCardFrontLayout.setVisibility(INVISIBLE);

                        if (onFlipListener != null)
                            onFlipListener.onViewFlipCompleted(EasyFlipView.this, FlipState.BACK_SIDE);
                        // Auto Flip Back
                        if (autoFlipBack == true) {
                            eventHandler = new EventHandler(EventRunner.getMainEventRunner());
                            eventHandler.postTimingTask(new Runnable() {
                                @Override
                                public void run() {
                                    flipTheView();
                                }
                            }, autoFlipBackTime);
                        }
                    }
                }

                @Override
                public void onPause(Animator animator) {
                }

                @Override
                public void onResume(Animator animator) {
                }
            });
            setFlipDuration(flipDuration);
        }
    }

    private void changeCameraDistance() {
        int distance = 8000;
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        float scale = display.getAttributes().scalDensity * distance;
        if (mCardFrontLayout != null) {
            mCardFrontLayout.setCenterZoomFactor(scale, scale);
        }
        if (mCardBackLayout != null) {
            mCardBackLayout.setCenterZoomFactor(scale, scale);
        }
    }

    /**
     * Play the animation of flipping and flip the view for one side!
     */
    public void flipTheView() {
        if (!flipEnabled || getChildCount() < 2) return;
        if (flipOnceEnabled && mFlipState == FlipState.BACK_SIDE)
            return;
        if (flipType.equalsIgnoreCase("horizontal")) {
            if (mSetRightOut.isRunning() || mSetLeftIn.isRunning()) return;
            mCardBackLayout.setVisibility(VISIBLE);
            mCardFrontLayout.setVisibility(VISIBLE);

            if (mFlipState == FlipState.FRONT_SIDE) {
                mSetRightOut = animatorUtil.animFlipHorizontalOut(mCardFrontLayout, flipDuration);
                mSetLeftIn = animatorUtil.animFlipHorizontalIn(mCardBackLayout, flipDuration);
                mSetRightOut.start();
                mSetLeftIn.start();
                mIsBackVisible = true;
                mFlipState = FlipState.BACK_SIDE;
            } else {
                mSetRightOut = animatorUtil.animFlipHorizontalOut(mCardBackLayout, flipDuration);
                mSetLeftIn = animatorUtil.animFlipHorizontalIn(mCardFrontLayout, flipDuration);
                mSetRightOut.start();
                mSetLeftIn.start();
                mIsBackVisible = false;
                mFlipState = FlipState.FRONT_SIDE;
            }
        } else {
            if (mSetTopOut.isRunning() || mSetBottomIn.isRunning()) return;
            mCardBackLayout.setVisibility(VISIBLE);
            mCardFrontLayout.setVisibility(VISIBLE);
            if (mFlipState == FlipState.FRONT_SIDE) {
                // From front to back
                mSetTopOut = animatorUtil.animFlipVerticalFrontOut(mCardFrontLayout, flipDuration);
                mSetBottomIn = animatorUtil.animFlipVerticalFrontIn(mCardBackLayout, flipDuration);
                mSetTopOut.start();
                mSetBottomIn.start();
                mIsBackVisible = true;
                mFlipState = FlipState.BACK_SIDE;
            } else {
                // from back to front
                mSetTopOut = animatorUtil.animFlipVerticalFrontOut(mCardBackLayout, flipDuration);
                mSetBottomIn = animatorUtil.animFlipVerticalFrontIn(mCardFrontLayout, flipDuration);
                mSetTopOut.start();
                mSetBottomIn.start();
                mIsBackVisible = false;
                mFlipState = FlipState.FRONT_SIDE;
            }
        }
    }

    /**
     * Flip the view for one side with or without animation.
     *
     * @param withAnimation true means flip view with animation otherwise without animation.
     */
    public void flipTheView(boolean withAnimation) {
        if (getChildCount() < 2) return;

        if (flipType.equalsIgnoreCase("horizontal")) {
            if (!withAnimation) {
                mSetLeftIn.setDuration(0);
                mSetRightOut.setDuration(0);
                boolean oldFlipEnabled = flipEnabled;
                flipEnabled = true;

                flipTheView();

                mSetLeftIn.setDuration(flipDuration);
                mSetRightOut.setDuration(flipDuration);
                flipEnabled = oldFlipEnabled;
            } else {
                flipTheView();
            }
        } else {
            if (!withAnimation) {
                mSetBottomIn.setDuration(0);
                mSetTopOut.setDuration(0);
                boolean oldFlipEnabled = flipEnabled;
                flipEnabled = true;

                flipTheView();

                mSetBottomIn.setDuration(flipDuration);
                mSetTopOut.setDuration(flipDuration);
                flipEnabled = oldFlipEnabled;
            } else {
                flipTheView();
            }
        }
    }

    /**
     * Whether view is set to flip on touch or not.
     *
     * @return true or false
     */
    public boolean isFlipOnTouch() {
        return flipOnTouch;
    }

    /**
     * Set whether view should be flipped on touch or not!
     *
     * @param flipOnTouch value (true or false)
     */
    public void setFlipOnTouch(boolean flipOnTouch) {
        this.flipOnTouch = flipOnTouch;
    }

    /**
     * Returns duration of flip in milliseconds!
     *
     * @return duration in milliseconds
     */
    public int getFlipDuration() {
        return flipDuration;
    }

    /**
     * Sets the flip duration (in milliseconds)
     *
     * @param flipDuration duration in milliseconds
     */
    public void setFlipDuration(int flipDuration) {
        this.flipDuration = flipDuration;
    }

    /**
     * Returns whether view can be flipped only once!
     *
     * @return true or false
     */
    public boolean isFlipOnceEnabled() {
        return flipOnceEnabled;
    }

    /**
     * Enable / Disable flip only once feature.
     *
     * @param flipOnceEnabled true or false
     */
    public void setFlipOnceEnabled(boolean flipOnceEnabled) {
        this.flipOnceEnabled = flipOnceEnabled;
    }


    /**
     * Returns whether flip is enabled or not!
     *
     * @return true or false
     */
    public boolean isFlipEnabled() {
        return flipEnabled;
    }

    /**
     * Enable / Disable flip view.
     *
     * @param flipEnabled true or false
     */
    public void setFlipEnabled(boolean flipEnabled) {
        this.flipEnabled = flipEnabled;
    }

    /**
     * Returns which flip state is currently on of the flip view.
     *
     * @return current state of flip view
     */
    public FlipState getCurrentFlipState() {
        return mFlipState;
    }

    /**
     * Returns true if the front side of flip view is visible.
     *
     * @return true if the front side of flip view is visible.
     */
    public boolean isFrontSide() {
        return (mFlipState == FlipState.FRONT_SIDE);
    }

    /**
     * Returns true if the back side of flip view is visible.
     *
     * @return true if the back side of flip view is visible.
     */
    public boolean isBackSide() {
        return (mFlipState == FlipState.BACK_SIDE);
    }

    public OnFlipAnimationListener getOnFlipListener() {
        return onFlipListener;
    }

    public void setOnFlipListener(OnFlipAnimationListener onFlipListener) {
        this.onFlipListener = onFlipListener;
    }

    /**
     * Returns true if the Flip Type of animation is Horizontal?
     */
    public boolean isHorizontalType() {
        return flipType.equals("horizontal");
    }

    /**
     * Returns true if the Flip Type of animation is Vertical?
     */
    public boolean isVerticalType() {
        return flipType.equals("vertical");
    }

    /**
     * Sets the Flip Type of animation to Horizontal
     */
    public void setToHorizontalType() {
        flipType = "horizontal";
        loadAnimations();
    }

    /**
     * Sets the Flip Type of animation to Vertical
     */
    public void setToVerticalType() {
        flipType = "vertical";
        loadAnimations();
    }

    /**
     * Sets the flip type from direction to right
     */
    public void setFlipTypeFromRight() {
        if (flipType.equals("horizontal"))
            flipTypeFrom = "right";
        else flipTypeFrom = "front";
        loadAnimations();
    }

    /**
     * Sets the flip type from direction to left
     */
    public void setFlipTypeFromLeft() {
        if (flipType.equals("horizontal"))
            flipTypeFrom = "left";
        else flipTypeFrom = "back";
        loadAnimations();
    }

    /**
     * Sets the flip type from direction to front
     */
    public void setFlipTypeFromFront() {
        if (flipType.equals("vertical"))
            flipTypeFrom = "front";
        else flipTypeFrom = "right";
        loadAnimations();
    }

    /**
     * Sets the flip type from direction to back
     */
    public void setFlipTypeFromBack() {
        if (flipType.equals("vertical"))
            flipTypeFrom = "back";
        else flipTypeFrom = "left";
        loadAnimations();
    }

    /**
     * Returns the flip type from direction. For horizontal, it will be either right or left and for vertical, it will be front or back.
     */
    public String getFlipTypeFrom() {
        return flipTypeFrom;
    }

    /**
     * Returns true if Auto Flip Back is enabled
     */
    public boolean isAutoFlipBack() {
        return autoFlipBack;
    }

    /**
     * Set if the card should be flipped back to original front side.
     *
     * @param autoFlipBack true if card should be flipped back to froont side
     */
    public void setAutoFlipBack(boolean autoFlipBack) {
        this.autoFlipBack = autoFlipBack;
    }

    /**
     * Return the time in milliseconds to auto flip back to original front side.
     *
     * @return
     */
    public int getAutoFlipBackTime() {
        return autoFlipBackTime;
    }

    /**
     * Set the time in milliseconds to auto flip back the view to the original front side
     *
     * @param autoFlipBackTime The time in milliseconds
     */
    public void setAutoFlipBackTime(int autoFlipBackTime) {
        this.autoFlipBackTime = autoFlipBackTime;
    }

    /**
     * The Flip Animation Listener for animations and flipping complete listeners
     */
    public interface OnFlipAnimationListener {
        /**
         * Called when flip animation is completed.
         *
         * @param newCurrentSide After animation, the new side of the view. Either can be
         *                       FlipState.FRONT_SIDE or FlipState.BACK_SIDE
         */
        void onViewFlipCompleted(EasyFlipView easyFlipView, FlipState newCurrentSide);
    }

    public String toEasyFlipViewString() {
        return "EasyFlipView{" +
                "flipOnTouch='" + flipOnTouch + '\'' +
                ", flipDuration='" + flipDuration + '\'' +
                ", flipEnabled=" + flipEnabled + '\'' +
                ", flipOnceEnabled=" + flipOnceEnabled + '\'' +
                ", autoFlipBack=" + autoFlipBack + '\'' +
                ", autoFlipBackTime=" + autoFlipBackTime +
                '}';
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN: {
                if (isEnabled() && flipOnTouch) {
                    return true;
                }
            }
            case TouchEvent.PRIMARY_POINT_UP: {
                if (isEnabled() && flipOnTouch) {
                    flipTheView();
                    return true;
                }
            }
        }
        return false;
    }
}
