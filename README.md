# EasyFlipView

**本项目是基于开源项目EasyFlipView进行ohos化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/wajahatkarim3/EasyFlipView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：EasyFlipView
- 所属系列：ohos的第三方组件适配移植
- 功能：一个快速简单的翻转视图，可以用来创建像信用卡，扑克卡等两面的视图。
- 项目移植状态：完成
- 调用差异：ohos属性动画方式不支持绕x轴，y轴旋转。暂时用旋转代替。
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/wajahatkarim3/EasyFlipView
- 原项目基线版本：3.0.0 ,sha1:c36b52002909ca07c1141985e23f14a83c856d8b
- 编程语言：Java
- 外部库依赖：无

#### 展示效果

![avatar](screenshot/preview.gif)

#### 安装教程

方法1.

1. 编译依赖包SectionedRecyclerViewAdapter.har。
2. 启动 DevEco Studio，将依赖包导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.wajahatkarim3.ohos:EasyFlipView:1.0.1'
}
```

#### 使用说明
#####  EasyFlipView In XM
layouts("Vertical")
```
<com.wajahatkarim3.easyflipview.EasyFlipView
	ohos:width="match_parent"
	ohos:height="match_content"
	app:flipOnTouch="true"
	app:flipEnabled="true"
	app:flipDuration="400"
	app:flipType="vertical"
	app:flipFrom="front"
	app:autoFlipBack="true"
	app:autoFlipBackTime="1000"
	>
	
	<!-- Back Layout Goes Here -->
     <Image
            ohos:id="$+id:king_back_side"
            ohos:height="500vp"
            ohos:width="match_parent"
            ohos:background_element="$media:back_side_king"
            />

        <Image
            ohos:id="$+id:king_front_side"
            ohos:height="500vp"
            ohos:width="match_parent"
            ohos:background_element="$media:front_side_design"
            />

</com.wajahatkarim3.easyflipview.EasyFlipView>
```
 layouts("Horizontal")
```
<com.wajahatkarim3.easyflipview.EasyFlipView
	ohos:width="match_parent"
	ohos:height="match_content"
	app:flipOnTouch="true"
	app:flipEnabled="true"
	app:flipDuration="400"
	app:flipFrom="right"
	app:flipType="horizontal"
	app:autoFlipBack="false"
	>

	<!-- Back Layout Goes Here -->
     <Image
            ohos:id="$+id:king_back_side"
            ohos:height="500vp"
            ohos:width="match_parent"
            ohos:background_element="$media:back_side_king"
            />

        <Image
            ohos:id="$+id:king_front_side"
            ohos:height="500vp"
            ohos:width="match_parent"
            ohos:background_element="$media:front_side_design"
            />
</com.wajahatkarim3.easyflipview.EasyFlipView>          
```
##### 方法实现 (Java)
```java
// Flips the view with or without animation
mYourFlipView.flipTheView();
mYourFlipView.flipTheView(false);

// Sets and Gets the Flip Animation Duration in milliseconds (Default is 400 ms)
mYourFlipView.setFlipDuration(1000);
int dur = mYourFlipView.getFlipDuration();

// Sets and gets the flip enable status (Default is true)
mYourFlipView.setFlipEnabled(false);
boolean flipStatus = mYourFlipView.isFlipEnabled();

// Sets and gets the flip on touch status (Default is true)
mYourFlipView.setFlipOntouch(false);
boolean flipTouchStatus = mYourFlipView.isFlipOnTouch();

// Get current flip state in enum (FlipState.FRONT_SIDE or FlipState.BACK_SIDE)
EasyFlipView.FlipState flipSide = mYourFlipView.getCurrentFlipState();

// Get whether front/back side of flip is visible or not.
boolean frontVal = mYourFlipView.isFrontSide();
boolean backVal = mYourFlipView.isBackSide();

// Get/Set the FlipType to FlipType.Horizontal
boolean isHorizontal = mYourFlipView.isHorizontalType();
mYourFlipView.setToHorizontalType();

// Get/Set the FlipType to FlipType.Vertical
boolean isVertical = mYourFlipView.isVerticalType();
mYourFlipView.setToVerticalType();

// Get/Set if the auto flip back is enabled
boolean isAutoFlipBackEnabled = mYourFlipView.isAutoFlipBack();
mYourFlipView.setAutoFlipBack(true);

// Get/Set the time in milliseconds (ms) after the view is auto flip back to original front side
int autoflipBackTimeInMilliseconds = mYourFlipView.getAutoFlipBackTime();
mYourFlipView.setAutoFlipBackTime(2000);

// Sets the animation direction from left (horizontal) and back (vertical)
easyFlipView.setFlipTypeFromLeft();

// Sets the animation direction from right (horizontal) and front (vertical)
easyFlipView.setFlipTypeFromRight();

// Sets the animation direction from front (vertical) and right (horizontal)
easyFlipView.setFlipTypeFromFront();

// Sets the animation direction from back (vertical) and left (horizontal)
easyFlipView.setFlipTypeFromBack();

// Returns the flip type from direction. For horizontal, it will be either right or left and for vertical, it will be front or back.
easyFlipView.getFlipTypeFrom();

```
翻转动画监听：

```java
EasyFlipView easyFlipView = (EasyFlipView) findViewById(R.id.easyFlipView);
easyFlipView.setOnFlipListener(new EasyFlipView.OnFlipAnimationListener() {
            @Override
            public void onViewFlipCompleted(EasyFlipView easyFlipView, EasyFlipView.FlipState newCurrentSide) {
                ToastDialog toastDialog = new ToastDialog(getContext());
                toastDialog.setText("Flip Completed! New Side is:"+ newCurrentSide).setDuration(1000).show();
            }
        });
```

定制化属性列表：

| Attribute Name                               | Default Value  | Description                                                  |
| -------------------------------------------- | -------------- | ------------------------------------------------------------ |
| app:flipOnTouch="true"                       | true           | Whether card should be flipped on touch or not.              |
| app:flipDuration="400"                       | 400            | The duration of flip animation in milliseconds.              |
| app:flipEnabled="true"                       | true           | If this is set to false, then it won't flip ever in Single View and it has to be always false for RecyclerView. |
| app:flipType="horizontal"                    | vertical       | Whether card should flip in vertical or horizontal.          |
| app:flipFrom="right"<br/>app:flipFrom="back" | left<br/>front | Whether card should flip from left to right Or right to left(Horizontal type) or car should flip to front or back(Vertical type). |
| app:autoFlipBack="true"                      | false          | If this is set to true, then he card will be flipped back to original front side after the time set in autoFlipBackTime. |
| app:autoFlipBackTime="1000"                  | 1000           | The time in milliseconds (ms), after the card will be flipped back to original front side. |


#### 版本迭代

- v1.0.1


#### 版权和许可信息

 Apache License, Version 2.0 

