package com.wajahatkarim3.easyflipview;

import ohos.aafwk.ability.AbilityForm;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.OnClickListener;
import ohos.aafwk.ability.ViewsStatus;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RecycleItemProvider;

import java.util.List;

public class SampleAdapter extends RecycleItemProvider {
    private List<TestModel> list;
    LayoutScatter layoutScatter;
    private AbilitySlice slice;

    public SampleAdapter(List<TestModel> list, AbilitySlice slice) {
        this.list = list;
        this.slice = slice;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        component = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_recyclerview, null, false);
        EasyFlipView flipView = (EasyFlipView) component.findComponentById(ResourceTable.Id_flipView);
        if (flipView.getCurrentFlipState() == EasyFlipView.FlipState.FRONT_SIDE && list.get(
                position).isFlipped) {
            flipView.setFlipDuration(0);
            flipView.flipTheView();
        } else if (flipView.getCurrentFlipState() == EasyFlipView.FlipState.BACK_SIDE
                && !list.get(position).isFlipped) {
            flipView.setFlipDuration(0);
            flipView.flipTheView();
        }
        flipView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (list.get(position).isFlipped) {
                    list.get(position).isFlipped = false;
                } else {
                    list.get(position).isFlipped = true;
                }
                flipView.setFlipDuration(700);
                flipView.flipTheView();
            }
        });
        return component;
    }
}
