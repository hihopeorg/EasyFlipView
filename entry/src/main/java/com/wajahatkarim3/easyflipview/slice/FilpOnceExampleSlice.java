package com.wajahatkarim3.easyflipview.slice;

import com.wajahatkarim3.easyflipview.EasyFlipView;
import com.wajahatkarim3.easyflipview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;

import static com.wajahatkarim3.easyflipview.EasyFlipView.label;

public class FilpOnceExampleSlice extends AbilitySlice {

    private DirectionalLayout myLayout = new DirectionalLayout(this);
    private Image king_back_side;
    private Image king_front_side;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_flip_once_example);
        initView();
    }

    private void initView() {
        final EasyFlipView easyFlipView = (EasyFlipView) findComponentById(ResourceTable.Id_cardFlipView);
        easyFlipView.setFlipDuration(1000);
        easyFlipView.setFlipEnabled(true);

        king_back_side = (Image) findComponentById(ResourceTable.Id_king_back_side);
        king_front_side = (Image) findComponentById(ResourceTable.Id_king_front_side);

        king_back_side.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ToastDialog toastDialog = new ToastDialog(getContext());
                toastDialog.setText("Front Card").setDuration(500).show();
                easyFlipView.flipTheView();
            }
        });

        king_front_side.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ToastDialog toastDialog = new ToastDialog(getContext());
                toastDialog.setText("Back Card").setDuration(500).show();
                easyFlipView.flipTheView();
            }
        });

        easyFlipView.setOnFlipListener(new EasyFlipView.OnFlipAnimationListener() {
            @Override
            public void onViewFlipCompleted(EasyFlipView easyFlipView, EasyFlipView.FlipState newCurrentSide) {
                ToastDialog toastDialog = new ToastDialog(getContext());
                toastDialog.setText("Flip Completed! New Side is:" + newCurrentSide).setDuration(1000).show();
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
