package com.wajahatkarim3.easyflipview.slice;

import com.wajahatkarim3.easyflipview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;

public class MainAbilitySlice extends AbilitySlice {

    private DirectionalLayout myLayout = new DirectionalLayout(this);
    private Button btSimpleView;
    private Button btRecyclerView;
    private Button btFlipOnceEg;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        btSimpleView = (Button) findComponentById(ResourceTable.Id_btSimpleView);
        btRecyclerView = (Button) findComponentById(ResourceTable.Id_btRecyclerView);
        btFlipOnceEg = (Button) findComponentById(ResourceTable.Id_btFlipOnceEg);
        intiClickedListener();
    }

    private void intiClickedListener() {
        btSimpleView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                AbilitySlice slice = new SimpleViewFlipAbilitySlice();
                Intent intent = new Intent();
                present(slice, intent);
            }
        });
        btRecyclerView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                AbilitySlice slice = new RecyclerViewFlipSlice();
                Intent intent = new Intent();
                present(slice, intent);
            }
        });
        btFlipOnceEg.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                AbilitySlice slice = new FilpOnceExampleSlice();
                Intent intent = new Intent();
                present(slice, intent);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
