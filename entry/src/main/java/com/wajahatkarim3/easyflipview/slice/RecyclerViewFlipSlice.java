package com.wajahatkarim3.easyflipview.slice;

import com.wajahatkarim3.easyflipview.ResourceTable;
import com.wajahatkarim3.easyflipview.SampleAdapter;
import com.wajahatkarim3.easyflipview.TestModel;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewFlipSlice extends AbilitySlice {

    private List<TestModel> list;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_recyclerview_flip);
        initData();
        SampleAdapter myItemProvider = new SampleAdapter(list, this);
        TableLayoutManager manager = new TableLayoutManager();
        manager.setColumnCount(2);
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_recyclerView);
        listContainer.setItemProvider(myItemProvider);
        listContainer.setLayoutManager(manager);
    }

    private void initData() {
        list = new ArrayList<TestModel>();
        for (int i = 0; i < 20; i++) {
            TestModel model = new TestModel();
            model.isFlipped = false;
            list.add(model);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
