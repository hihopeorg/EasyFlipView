package com.wajahatkarim3.easyflipview.slice;

import com.wajahatkarim3.easyflipview.EasyFlipView;
import com.wajahatkarim3.easyflipview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;

import static com.wajahatkarim3.easyflipview.EasyFlipView.label;

public class SimpleViewFlipAbilitySlice extends AbilitySlice {

    private DirectionalLayout myLayout = new DirectionalLayout(this);
    private Image imgBackCard;
    private Image imgFrontCard;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_simple_view);
        initView();
    }

    private void initView() {
        final EasyFlipView easyFlipView = (EasyFlipView) findComponentById(ResourceTable.Id_easyFlipView);
        easyFlipView.setFlipDuration(400);
        easyFlipView.setFlipEnabled(true);

        imgBackCard = (Image) findComponentById(ResourceTable.Id_imgBackCard);
        imgFrontCard = (Image) findComponentById(ResourceTable.Id_imgFrontCard);

        imgFrontCard.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ToastDialog toastDialog = new ToastDialog(getContext());
                toastDialog.setText("Front Card").setDuration(500).show();
                easyFlipView.flipTheView();
            }
        });

        imgBackCard.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ToastDialog toastDialog = new ToastDialog(getContext());
                toastDialog.setText("Back Card").setDuration(500).show();
                easyFlipView.flipTheView();
            }
        });

        easyFlipView.setOnFlipListener(new EasyFlipView.OnFlipAnimationListener() {
            @Override
            public void onViewFlipCompleted(EasyFlipView easyFlipView, EasyFlipView.FlipState newCurrentSide) {
                ToastDialog toastDialog = new ToastDialog(getContext());
                toastDialog.setText("Flip Completed! New Side is:"+ newCurrentSide).setDuration(1000).show();
            }
        });

        final EasyFlipView easyFlipView2 = (EasyFlipView) findComponentById(ResourceTable.Id_easyFlipView2);
        easyFlipView2.setFlipDuration(400);
        easyFlipView2.setToHorizontalType();
        easyFlipView2.setFlipTypeFromLeft();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
