package com.wajahatkarim3.easyflipview;

import com.wajahatkarim3.easyflipview.utils.EventHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Button;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import org.junit.After;
import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ExampleOhosTest {
    @After
    public void tearDown() {
        EventHelper.clearAbilities();
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.wajahatkarim3.easyflipview", actualBundleName);
    }

    @Test
    public void testFilpOnceExampleSlice() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("testSwipe failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EasyFlipView easyFlipView =
                (EasyFlipView) mainAbility.findComponentById(ResourceTable.Id_cardFlipView);
        EventHelper.triggerClickEvent(mainAbility,easyFlipView);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //assertEquals("CheckedBackgroundColor,  value",  easyFlipView.getFlipDuration(), 500);
    }

    @Test
    public void testbtSimpleView() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("testSwipe failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Button button =
                (Button) mainAbility.findComponentById(ResourceTable.Id_btSimpleView);
        EventHelper.triggerClickEvent(mainAbility,button);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EasyFlipView easyFlipView = (EasyFlipView) mainAbility.findComponentById(ResourceTable.Id_cardFlipView);
        EventHelper.triggerClickEvent(mainAbility,easyFlipView);
    }

    @Test
    public void testbtRecyclerView() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("testSwipe failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Button button =
                (Button) mainAbility.findComponentById(ResourceTable.Id_btRecyclerView);
        EventHelper.triggerClickEvent(mainAbility,button);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ListContainer easyFlipView = (ListContainer) mainAbility.findComponentById(ResourceTable.Id_recyclerView);
        EventHelper.triggerClickEvent(mainAbility,easyFlipView);
    }

    @Test
    public void testbtbtFlipOnceEg() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("testSwipe failed, can not start ability", mainAbility);
        EventHelper.waitForActive(mainAbility, 5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Button button =
                (Button) mainAbility.findComponentById(ResourceTable.Id_btFlipOnceEg);
        EventHelper.triggerClickEvent(mainAbility,button);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EasyFlipView easyFlipView = (EasyFlipView) mainAbility.findComponentById(ResourceTable.Id_cardFlipView);
        EventHelper.triggerClickEvent(mainAbility,easyFlipView);
    }
}