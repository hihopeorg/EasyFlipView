package com.wajahatkarim3.library;

import com.wajahatkarim3.easyflipview.EasyFlipView;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

public class EasyFlipViewTest {

    @Test
    public void initAttrWithNull() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        EasyFlipView easyFlipView = new EasyFlipView(context);
        easyFlipView.initAttr(context,null);
        Assert.assertNotNull("It is ok",easyFlipView);
    }

    @Test
    public void toEasyFlipViewString() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        EasyFlipView easyFlipView = new EasyFlipView(context);
        easyFlipView.toEasyFlipViewString();
        Assert.assertNotNull("It is ok",easyFlipView);
    }

    @Test
    public void flipTheViewWithFalse() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        EasyFlipView easyFlipView = new EasyFlipView(context);
        easyFlipView.setFlipEnabled(false);
        easyFlipView.flipTheView();
        Assert.assertNotNull("It is ok",easyFlipView);
    }

    @Test
    public void flipTheView() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        EasyFlipView easyFlipView = new EasyFlipView(context);
        easyFlipView.setFlipEnabled(true);
        easyFlipView.flipTheView();
        Assert.assertNotNull("It is ok",easyFlipView);
    }
}